export class DefaultContractValues {
    public id: number;
    public dailyAmount: number;
    public dateChange: Date;
    public gouterAmount: number;
    public grossHourly: number;
    public mealAmount: number;
    public netHourly: number;
    public vehicleAmount: number;

    constructor() {
    }
}
