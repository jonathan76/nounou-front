import { Person } from '../classes/persons';

export class TypePerson {
    public id: number;
    public name: string;
    public persons: Person[];
}
