// User's roles
export interface IRole {
    id: number;
    name: string;
}
