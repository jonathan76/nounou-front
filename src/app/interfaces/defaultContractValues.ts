/**
 * Interface for a user of the application
 */
export interface IDefaultContractValues {
    id: number;
    dailyAmount: number;
    dateChange: Date;
    gouterAmount: number;
    grossHourly: number;
    mealAmount: number;
    netHourly: number;
    vehicleAmount: number;
}
