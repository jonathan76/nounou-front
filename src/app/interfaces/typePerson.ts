import { IPerson } from '../interfaces/person';

export interface ITypePerson {
    id: number;
    name: string;
    persons: IPerson[];
}
